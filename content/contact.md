---
draft: false
headline: contact
subtitle: ""
url: contact
title: contact
weight: 3
linkTitle: contact
description: My contact details
date: 2021-01-05
menu: main
---

Let's get in touch!

* You can reach me on my personal email: hello @ fanid.gr

Mailto and my PGP key:

[{{< fa fas envelope 2x >}}](mailto:hello@fanid.gr)[{{< fa fas key 2x >}}](https://keys.openpgp.org/search?q=hello%40fanid.gr)

* You can also find me and my projects on the following accounts:

[{{< fa fab github 2x >}}](https://github.com/FaniD)[{{< fa fab gitlab 2x >}}](https://gitlab.com/FaniD)[{{< fa fab linkedin 2x >}}](https://linkedin.com/in/fanid)

If you like my work or found something helpful among my projects and want to support me, you can 'buy me a coffee' here:

{{< figure class="image main" src="images/uploads/paypal.png" title="Click Here" link="https://paypal.me/fanidim" >}}

