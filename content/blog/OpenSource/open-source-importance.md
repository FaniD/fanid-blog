---
draft: false
headline: Open Source Importance
date: 2020-06-19
slug: open-source-importance
url: open-source-importance
title: Why is Open Source Important?
weight: 9
linkTitle: Open Source Importance
---
*19 June 2020*
{{< figure class="image main" src="../../images/uploads/open.jpg" >}}
Photo by Richard Balog // [Unsplash](https://unsplash.com/photos/H9GZgI6jU7Y)

> This blog post holds a special place in my heart, as I wrote it for LaceWing Tech and it was originally published at our blog. It was a contribution I made to support the company in Corona times. So this post brings back memories from that awesome team and an one of a kind company where it was my honour to work.

Open Source software follows a fundamental rule: code is publicly accessible and freely distributed to be used or modified by anyone.

If you’re making your first steps into the Open Source world, you may think that you’ve never used Open Source software before. The truth is that Open Source is all around us, and sometimes you’re using it without even realizing it. For example, do you use Firefox or Chrome as your browser? If so, you are using software from popular open source projects.

## Who benefits from Open Source?

Open Source offers different benefits, depending on the perspective from which you look at it. An individual user benefits from it directly every day while using it. In general, the adaption of Open Source has a positive impact on the quality and the development of technology itself, too. Also, when you look at the whole picture, society benefits from it in a special way – and this might be the most important benefit that Open Source offers.

### Why you should use Open Source software

If you are wondering how you as an individual could benefit from using Open Source software instead of a Closed Source alternative, you’re headed in the right direction. Everybody starts with that thought the first time they get involved with Open Source.

#### It’s free and yet powerful

Closed Source software is often believed to be superior to Open Source alternatives, because of the money you invest in order to use it. This is a myth. Just because you don’t pay for it, it does not mean that Open Source software has less features. On the contrary, people are contributing to Open Source projects because they need to use their software, so they do their best to make it functional and feature-rich.

#### Modify according to your taste

A company that produces a Closed Source product invests a lot of money to keep their clients happy. This means that the software is developed to satisfy their clients’ needs and their needs only. Most of the times, these clients are companies that are buying big enterprise edition installations.  So, when you’re using your personal Closed Source software installation there may be features that you don’t like – but there´s not much you can do about it. In the open source world, if you don’t like something, you can change it so that it fits your needs.

#### “Do I have to know how to code in order to do it?”

Not necessarily! If you’re into coding then you can change whatever you don’t like on your own. However, that’s not the only way to trigger a change. Keep in mind that almost every open source project has a community where people make code contributions and are willing to help others contribute too. A contribution is not restricted to coding. You’re more than welcome to report a bug or request a new feature and it’s very likely that others will help you implement it.

### How does Open Source improve technology?

#### General technology enhancement

When a group of people use an Open Source software every day and test it to its limits, they discover bugs and come up with new feature ideas. They will contribute code to fix the bugs and the community will embrace and include it in the upcoming release.

#### “Does that mean that the software itself is the only one who wins?”

No! All technology in general advances, because when people from several communities work together and distribute code, great achievements are made and they are not restricted to one software’s code. New ideas are born publicly, recycled and integrated by other projects. It’s not a contribution to a single software or a company, it’s a contribution to technology as a whole. One great example of that is Security. When there is a security hole in the code of a project and one team finds it, another project could detect the same hole on their project and use the same piece of code to fix it. That means that security in general is enhanced, which is a good thing overall.

#### Code is company independent

Another benefit of Open Source is the decoupling of software from a single company. When a software is supported explicitly by one company, and the company goes out of business, then so too does the software. That’s not the case with open source. If a company that produces an Open Source software goes out of business, the code can be still used by others and if the license allows it, another company or community can take over and continue supporting the project.  Open Source software is not bound to one single company; rather, it is free technology to be used and extended by anyone.

### How society benefits from Open Source

In addition to the practical benefits that Open Source offers to individuals and technology, some political benefits arise from its philosophy.

#### Equal opportunities

It’s a fact that not everybody has equal privileges in many aspects of life, including technology and information. Open Source makes a huge step towards the revolution we need in order to achieve social justice, by offering people free access to technology . It narrows the social inequalities gap, as far as technology is concerned .

When software or hardware (yes, there is open source hardware, too) is distributed equally to people, then we’re one step closer to having equal opportunities to learning and getting familiar with tech tools and therefore, equal opportunities to education, employment and so on.

#### Public goods systems should use public code

Another way that society could benefit from Open Source is transparency and fair distribution of public goods. That could be achieved by using Open Source software in all public systems . A democratic society should demand public code in every public system  so that people know how their data is used/processed.

Also, using Open Source software could also result in improving a public system. For example, the public health system could be improved by using Open Source software so that health professionals and authorities share their experience with each other and knowledge is distributed to everybody, and not only institutions with more resources than others.

### Conclusion

From whichever perspective that you look at it, Open Source is important. It helps people, it improves technology and last but not least, it provides some humanitarian benefits. All it takes is that more people use it, because they are the lifeblood of Open Source.

- - -

Recommended reading: **["Roadwork ahead - Evaluating the needs of FOSS communities working on digital infrastructure in the public interest"](https://recommendations.implicit-development.org/)**

*"This report explores the unique nature of the community that builds and maintains free and open-source software (FOSS) and the role that community takes in shaping an open Internet. It draws on interviews with contributors to FOSS projects to highlight the community’s strengths as well as its challenges."*
