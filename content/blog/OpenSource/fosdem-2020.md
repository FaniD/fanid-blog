---
draft: false
headline: FOSDEM 2020
date: 2020-02-15
slug: fosdem-2020
url: fosdem-2020
title: FOSDEM 2020
weight: 11
linkTitle: Open Source Importance
---
*15 February 2020*
{{< figure class="image main" src="../../images/uploads/fosdem2020/nextcloudteam.jpg" >}}

I’ve always been hearing stories about this open source fiesta called FOSDEM, but I never had the chance to attend to. This year, Nextcloud made that dream come true. Thank you [Nextcloud](https://nextcloud.com) for sponsoring me to this unique experience!

FOSDEM conference takes place at Brussels and it lasts for 2 days. However, the whole week in town is full of numerous open source events. I would be staying for 4 days, to attend some events and do some sightseeing since it was the first time I visited Brussels. So... Luggage ready, camera ready, open source mode on and we’re off!

{{< figure class="image main" src="../../images/uploads/fosdem2020/travel.jpg" >}}

I travelled along with my friend [Stathis](https://eiosifidis.blogspot.com), and arriving at FOSDEM we met the rest of the Nextclouders. We were both glad to see again people we knew - Frank, Jos, Marinela, Greta - but we also made some new acquaintances - Bjoern, Silva, Borris. Altogether, we helped at the booth, talking to people about Nextcloud, introducing it to people that have never heard of it, spreading the word about Nextcloud Hub and helping others resolve any issues they had. And of course, handing out stickers generously!

The booth this year had grown bigger - that explains why Nextcloud was the only organization to have two tables 😉 And a cool blue light on the background!

{{< figure class="image main" src="../../images/uploads/fosdem2020/booth.jpg" >}}

Nextcloud shared the booth with [DAVx5](https://www.davx5.com/) and [ONLYOFFICE](https://www.onlyoffice.com/). It was a pleasure sharing the booth with these awesome people and I’m very happy I got to know them! Open source is literally connecting people 

Both days, FOSDEM was quite crowded and we’ve talked to an amazingly large amount of people. As of the talks, I attended a few - containers related topics - but the rooms were overcrowded and sound didn’t work too well because of that. For me, FOSDEM was more about networking and getting to know people from open source community. And of course, collecting some stickers!

If you feel like seeing some Fosdem Faces to get the feeling, take a look at [this album](http://toujoursdeja.fr/projects/faces-of-fosdem/2020.html). The photographer got my face too 

One of the events we visited on our trip was that of FSFE. It was pretty interesting to listen to people from different countries talking about their way of promoting open source technologies to their country. That’s where we met Izabel and Luis. My “most important people” from this trip 

{{< figure class="image main" src="../../images/uploads/fosdem2020/on-our-way-to-delirium.jpg" title="Stathis, Me, Izabel, Luis on our way to Delirium" >}}

Izabel is one of Stathis’s friends from OpenSUSE community and Luis, is the founder of GNUHealth project. [GNUHealth](https://www.gnuhealth.org) was the first Free Software project that clearly focused on Public Health and Social Medicine. Rumor has it that Luis’ experience in rural and underprivileged areas in South America led him to start thinking on how Free Software could help health professionals and authorities to improve the public health system. That’s how he created GNUHealth, a "Social Project with some technology behind" as he defines it in [this talk](<https://media.libreplanet.org/u/libreplanet/m/free-software-as-a-catalyst-for-liberation-social-justice-and-social-medicine/). For me Luis, is exactly the type of person the world needs most. I was so impressed by his story and his project and I definitely want to find a way to contribute to it in the future.

Back to Brussels, along with these people we headed to Delirium to get a beer with some OpenSUSE friends.

{{< figure class="image main" src="../../images/uploads/fosdem2020/delirium.JPG" >}}

Since I stayed a bit longer in Brussels I had some time to walk around town, do some sightseeing and meet old friends. The most popular spots in the city is the Grand-Place square and the view from Mont des Art. As of attractions, a must-see is definitely Atomium and maybe these well known pissing statues (Manneken Pis, Jaeneken pis, Zinneke Pis). But if you ever visit Brussels, I suggest you take a quick tour around and then visit the cities nearby, like Gent or Bruges. And don’t forget to taste some fries and waffles!

Until next time, enjoy some photos of the city 🙂

{{< figure class="image main" src="../../images/uploads/fosdem2020/atomium.JPG" title="Atomium" >}}

{{< figure class="image main" src="../../images/uploads/fosdem2020/mont-des-arts.jpg" title="Mont des Arts" >}}

{{< figure class="image main" src="../../images/uploads/fosdem2020/gavas.jpg" title="Met my old friend, Alexandros" >}}

{{< figure class="image main" src="../../images/uploads/fosdem2020/gent3.jpg" title="Gent" >}}

{{< figure class="image main" src="../../images/uploads/fosdem2020/gent2.jpg" title="Gent" >}}

{{< figure class="image main" src="../../images/uploads/fosdem2020/gent1.jpg" title="Gent" >}}

{{< figure class="image main" src="../../images/uploads/fosdem2020/basilica.JPG" title="Basilica of the Sacred Heart" >}}

{{< figure class="image main" src="../../images/uploads/fosdem2020/maneken.JPG" title="Manneken Pis" >}}

{{< figure class="image main" src="../../images/uploads/fosdem2020/zineken.jpg" title="Zinneke Pis" >}}

{{< figure class="image main" src="../../images/uploads/fosdem2020/waffles.jpg" title="Best waffles in town" >}}

