---
draft: false
headline: Ansiblize NextcloudPi
date: 2019-08-27
slug: ansible
url: ansiblize-nextcloudpi
title: Ansiblize NCP
weight: 6
linkTitle: Ansiblize NCP
---
*27 August 2019*
{{< figure class="image main" src="../../../images/uploads/ansiblize.png" >}}

One of the tasks I was assigned on GSoC 2019 was to convert the whole server of NextCloudPi into Ansible. Meaning, I had to convert some really complicated bash scripts into Ansible playbooks.

This proved out to be very time consuming for the simple reason that I had never worked with Ansible before, and although it is a handy tool especially for IT people, it has quite a large learning curve and for somebody who is used to coding, it may be stiff to convert code into yml files and get into its logic.

But through this, I now understand why so many IT people are adopting it. Thus, it may be useful for NextCloudPi to have available Ansible playbooks for ncp-config.

**Ansible** is a universal language, unraveling the mystery of how work gets done. Ansible can be used to configure a server, deploy an application or automate a task. It can also do IT orchestration, and simplify the process of executing sequential actions on multiple servers or devices.

Bash scripts are convenient to convert to ansible, as most of shell commands have a respective module in Ansible.

This task is still a work in progress, so you cannot use it just yet. By now, the two – big – sections of NextCloudPi server, CONFIG and SYSTEM, have been implemented. Each of the bash script in the sections of ncp-config, splits its functions into distinct Ansible playbooks. As soon as everything is turned into Ansible, ncp-config will be edited to use these playbook instead of the bash scripts.

Feel free to take a look at the [existing ansible playbooks](https://github.com/eellak/gsoc2019-NextCloudPi/tree/gsoc2019-ansible/Ansible)

What I found out in the process of writing Ansible playbooks was that I was going over previous yml files to re-use commands that I have written. Thus I decided to create my own inventory to help me move faster with this task.

A series of articles will follow as a tutorial to convert bash blocks of code to ansible tasks and eventually create an Ansible playbook out of a bash script. NO you can’t “learn Ansible in 5 min” with it (and probably with no other guide..), but it may make your life easier to get used to it and have some basic commands alltogether.

**Usage:** Using Ansible to replace your bash scripts is easy Just make sure you give the correct names to the arguments you’re passing so that they correspond to the variable inside an Ansible playbook.

So the execution of a bash script which is like the following command:

```
./bash_script.sh argument1 argument2
```

Will be replaced by the following Ansible command:

```
ansible-playbook playbooks_name.yml --extra-vars "version=argument1 other_variable=argument2"
```

If there are no arguments passing, you can skip the extra vars flag.

If the function replaced by ansible playbook is returning a value that you need, I have fixed it so that the following shell command will get you the returned value:

```
var=$(ansible-playbook playbook_name.yml | grep "return.std"
```

The same thing could be done by using the "fetch" option but that would require to convert the shell script so that it gets the input from the fetched folder.
