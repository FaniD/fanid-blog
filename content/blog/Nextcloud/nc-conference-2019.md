---
draft: false
headline: Nextcloud Conference 2019
date: 2019-09-30
slug: nc-conf-2019
url: nc-conf-2019
title: Nextcloud Conference 2019
weight: 7
linkTitle: Nextcloud Conference 2019
---
*30 September 2019*
{{< figure class="image main" src="../../images/uploads/nc-conf-2019/nc-conf-2019.jpg" >}}

After completing Google Summer of Code, I submitted a lightning talk about my work at the [Nextcloud Conference 2019](https://nextcloud.com/conf-2019/). Moreover, I asked to participate to the volunteers team along with my mentors, Pantelis and Stathis.

It was the first time I attended a conference abroad and the first time I ever visited Berlin. It was a unique experience from which I gained a lot and I feel grateful that Nextcloud sponsored me to join the conference.

Let me share this one of a kind experience with you and hopefully inspire more people to join the Nextcloud community and attend the following Nextcloud conferences!

Eirini, Stathis, Pantelis and me, all Greeks, all volunteers, visited the TU Berlin to help set up the main hacking room, in any way each of us was capable of. Tables, chairs, drinks, signs, registration bags, badges etc. All set for the big registration day. This is when we first met Jos and Marinela, who welcomed us, showed us how to organize the place and helped us with our tasks. Thank you both! 🙂

{{< figure class="image main" src="../../images/uploads/nc-conf-2019/volunteers-tasks.jpg" >}}

As time passed, more and more Nextclouders arrived at TU Berlin. One of the parts I enjoyed most during this trip, was meeting all these awesome people, who made the newcomers like me feel like home and with whom we had some constructive conversations, technical or not, and had a great time 🙂 I have to admit, I’m inspired by them and I want to contribute to Nextcloud now more than ever!

**Conference day 1** has come and all volunteers are on their shifts, exhibitors at their booths, speakers getting ready and TU Berlin is getting crowded by early in the morning.

Frank Karlitschek (CEO of Nextcloud) opened the conference presenting Nextcloud 17 and the new features coming with it. The shiny new features were pretty impressive, like **Remote Wipe** which allows users and administrators to forcibly clean files from remote devices (e.g. in case they are stolen) or the **two-factor authentication improvements** about the first login and new administrator settings, regarding lost or broken second factor solution and delegating the ability to create one-time-login tokens to group administrators. Moreover, some big achievements were announced. One of them is a doubling of **Nextcloud security** bug bounties to USD 10.000, which means we’re talking about a solid and reliable product. Another announcement that got me, was the collaboration with IBM. Nextcloud 17 introduces **IBM Spectrum Scale integration**, a high-performance file system, which results in several benefits in the area of performance, scalability and storage integration. You can read more about the new features [here](https://nextcloud.com/blog/nextcloud-17-scales-up-and-improves-data-protection-with-remote-wipe-collaborative-text-editor-2fa-updates-ibm-spectrum-scale-support-and-global-scale-improvements).

{{< figure class="image main" src="../../images/uploads/nc-conf-2019/frank.jpg" >}}

Later that day, among the lightning talks, I had my 5min presentation about my work on GSoC project “Expanding NextCloudPi”, which you can read [here](https://github.com/FaniD/Presentations/blob/master/Nextcloud_conference_2019/GSOC_Expanding_NCP.pdf) (or watch the video at 5:31:06).

Some of the talks that stood out for me were “The Not-so-secret future” of the American activist for Freedom of Expression, Jilian York as well as the lightning talk of the investigative reporter Fredrik Laurin about “The Investigative Cloud”.

{{< youtube id="494b_gEVgOc&t" >}}

A party followed at the Tiergarten where we ate some Berlin dishes and had fun getting to know new people.

**Conference day 2** was as interesting as day 1, but except for the technical talks, we listened to some talks that really broadens one’s horizons like the keynote by Renata Avila or Thomas Lohninger’s talk “Stand up and act!”. Both of these talks were breathtaking and inspiring.

{{< youtube id="9gaphVFtB3Q&t" >}}

At the end, Frank and Jos thanked the volunteers and the conference was officially over.

{{< figure class="image main" src="../../images/uploads/nc-conf-2019/volunteers.jpg" >}}

The rest of the days, a Hack Week took place at the lounge where Nextcloud employees, along with everyone who was willing to hack, shared ideas about Nextcloud and worked together on projects.

{{< figure class="image main" src="../../images/uploads/nc-conf-2019/lounge.jpg" >}}

As for Berlin, we did have some free time to go for sightseeing and visit some museums. We visited some of the mainstream attractions like the Brandenburg Gate or East Side Gallery, but we had also the chance to visit some alternative places like C-Base, where our friend Marie gave us a tour, or Teufelsberg, where we did some hiking into the woods carrying laptops with us – who said software engineers can’t do that? 😉

{{< figure class="image main" src="../../images/uploads/nc-conf-2019/greekteam.png" title="Greek dream team: Stathis, Fani, Pantelis, Eirini" >}}

Again, I want to thank Nextcloud and all of the team for this unique experience!
