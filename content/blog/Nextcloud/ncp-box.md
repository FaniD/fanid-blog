---
draft: false
headline: The revival of a Nextcloud Box
date: 2019-10-01
slug: ncp-box
url: ncp-bpx
title: The revival of a Nextcloud Box
weight: 8
linkTitle: The revival of a Nextcloud Box
---
*1 October 2019*
{{< figure class="image main" src="../../images/uploads/ncp-box/box.jpg" >}}

Throughout my work on NextCloudPi, I had to do some testing on the NCP board images. Sooner or later, my Raspberry Pi was on fire, constantly testing different versions and features of NCP. Every now and then I encountered articles about the Nextcloud Box, but when I searched for it I found out that it’s been sold out.

Recently, I visited Berlin to attend the [Nextcloud conference](#nc-conf-2019) and that’s where a Nextcloud Box case fell into my hands (Thanks Jan 🙂 ).

People kept telling me that it’s useless since the specific hard drive it was designed for, does not exist anymore. But as I was pretty grateful for this gift, I promised myself I would make it work no matter what.

Challenge accepted! Let’s solve the mystery of the Nextcloud Box.

{{< figure class="image main" src="../../images/uploads/ncp-box/ncbox.jpg" >}}

The case is designed to house a compute board (Raspberry Pi 2 or 3 or oDroid C2) and a 2.5 inch hard drive.

The hard drive that the Box was originally designed to use is the PiDrive, a Raspberry Pi-optimised USB hard drive. Unfortunately, Western Digital has shut down their research team WDLabs which produced the Box, thus the PiDrive is gone as well.

The main question I had to answer in order to find an alternative to replace the PiDrive was the following: **Why use a Raspberry Pi oriented hard drive in the first place?** One of the major benefits of Raspberry Pi – except for the price and portability – is that it requires low power: 5.0V +/- 5%. On the other hand, hard drives usually require more power than the Pi can supply, especially as they start (maybe an Amp or more). This could overload things and cause trouble (I learned that the hard way after destroying my RPi’s power supply…). So, what PiDrive actually does is requiring lower power consumption than standard 2.5″ HDD and utilising a custom cable which splits the power input. PiDrive’s cable is used to provide an organized power source and supply the appropriate power to the HDD when connected to a Raspberry Pi board. This cable combined with an external HDD would be the solution to the revival of the Box, but of course it’s sold out too, as the PiDrive itself.

After I figured out that the fundamental key of the PiDrive is the required power, I focused on finding a way to get a regular internal HDD the power it needed. The HDD I picked is a **2.5″ Seagate Barracuda 1TB**, which fits perfectly inside the case.

What we definitely need in order to connect the RPI with the HDD is a **USB to SATA Adapter**. PiDrive is using such an adapter as well but it’s integrated. Personally, as I often lack hardware, I create custom-made solutions. So in my case, I used an adapter of an old HDD case with a mini USB port.

Finding a mini USB to USB cable, seemed to be easy. However, the first attempt didn’t go very well, as I used a cable of a single USB 2.0 connector and this could not deliver the power the HDD needed. As a result, my RPi’s power supply died so..do not try this at home! What did the job, was a **cable of two USBs 2.0 connectors** (one for power and data, the other for power only) which worked just fine as the HDD could draw power from two USB ports simultaneously.

After replacing the **power supply for the RPi**, and getting an **ethernet cable** available, it was time to burn a nextcloud image to the **micro SD card**. Here comes the NextCloudPi 🙂

**NextCloudPi** is a Nextcloud instance that is preinstalled and preconfigured, and includes a management interface with all the tools you need to self host your private data in a single package. This is an [official](https://ownyourbits.com/2017/06/01/nextcloudpi-becomes-official/) open source community project that aims at making it easier for everyone to have control over their own data. You can read more about it [here](https://ownyourbits.com/nextcloudpi/) and download the images [here](https://ownyourbits.com/downloads/).

NextCloudPi constitutes the best solution for the Nextcloud Box as it is implemented to provide an option to store data and create backups directly to a USB drive.

Use balenaEtcher to burn the image to the SD card easily. As soon as you do this, insert the SD card to your compute board and plug in the power supply. The rest comes straightforward by using the wizard on your first run.

The following video shows the setup I described above alltogether.

{{< youtube id="zc4c-6oMe6E" >}}

It looks pretty, right? Let’s use it to test its functionality!

First, make sure that you know the IP address that your RPi is assigned. Then use it to enter NCP’s web interface through your browser. Make sure you get your password’s printed to file and continue to the activation.

{{< figure class="image main" src="../../images/uploads/ncp-box/1.png" >}}

The screenshots below will guide you step by step how to configure your NCP so that it uses the HDD as the data storage.

{{< figure class="image main" src="../../images/uploads/ncp-box/2.png" >}}

{{< figure class="image main" src="../../images/uploads/ncp-box/3.png" >}}

{{< figure class="image main" src="../../images/uploads/ncp-box/4.png" >}}

{{< figure class="image main" src="../../images/uploads/ncp-box/5.png" >}}

{{< figure class="image main" src="../../images/uploads/ncp-box/6.png" >}}

{{< figure class="image main" src="../../images/uploads/ncp-box/7.png" >}}

{{< figure class="image main" src="../../images/uploads/ncp-box/8.png" >}}

Let’s create a backup and check the drive for its existence. Make sure to enable SSH via NextCloudPi web panel first as it is disabled by default.

{{< figure class="image main" src="../../images/uploads/ncp-box/9.png" >}}

Use ssh to enter your RPi and ls the directory mentioned above.

{{< figure class="image main" src="../../images/uploads/ncp-box/10.png" >}}

There it is! The backup compressed as asked!

My NCP instance using a USB drive as data storage is ready to be used, perfectly fitted inside this little black box :)
