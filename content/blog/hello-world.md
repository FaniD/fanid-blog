---
draft: false
headline: Hello World
date: 2019-07-11
slug: welcome
url: hello-world
title: Hello World
weight: 1
linkTitle: Hello World
---
*11 July 2019*
{{< figure class="image main" src="../images/uploads/helloworld.jpg" >}}
Welcome to my blog!

Hello world and let this story begin...

I created this blog to house my thoughts – mainly – about software engineering related stuff and also anything else that sounds interesting to share – conferences, travels, books, movies or whatever.

Feel free to take a look at section [about me](/#about-me) to get an idea of $whoami.

I hope you will find something useful around here!
