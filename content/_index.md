---
title: Fani Dimou
description: Cloud enthusiast, religious about Linux, open-source supporter, passionate about security, systems person. Writes human-readable code. Likes automating stuff and being the captain of a container ship. Loves learning and sharing knowledge.
background: "images/mybg.jpg"
icon: "gem"
---
