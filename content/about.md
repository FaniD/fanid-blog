---
draft: false
headline: About me
subtitle: ""
url: about
title: About me
weight: 1
linkTitle: About
date: 2021-01-03
description: A few things about me
menu: main
---
Hey there! If you want to learn a few things about me, you’re in the right place... Go on reading!

My name is Fani Dimou and my pronouns are she/her. There are two places I call home, Athens where I'm originally from and Berlin where my work life lies.

{{< figure class="image main" src="images/uploads/fani.jpg" title="The Kiss - East Side Gallery" >}}

I've studied Electrical and Computer Engineering at National Technical
University of Athens and I enjoyed every piece of my studies there. Between those two fields, I've chosen the path of Computer Engineering and thus I became a software engineer. My interest lies mainly in the field of Cloud Computing and Distributed Systems as well as Operating Systems. Or practically: cloud infrastructure, automation, containers and container orchestration, devops/gitops etc.. However, I love learning about new topics and I am always interested on testing new technologies.

I hope you'll find something useful on my [blog](/blog/). If you feel like getting in touch, you can find my contact details [here](/#contact). You can also find my full CV [here](https://www.fanid.gr/files/cv.pdf).
